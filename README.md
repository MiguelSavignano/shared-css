## How share stylesheets (CSS)

Using npm you can share some stylesheets stored in bitbucket

## Install

### ssh
The host need a id_rsa with bitbucket permission
```
npm install git+ssh://bitbucket.org:MiguelSavignano/shared-css.git --save
```

### Use

```sass
// my_style.scss
@import "./node_modules/shared-css/main.scss";
// or
@import "./node_modules/shared-css/main.css";
```

### Create shared stylesheets

* Create a package.json with your module name
```
npm init
```
* Create folder with your css
* Create your css files inside folder
* Convert sass to css (Optional)

```
npx node-sass shared-css/main.scss shared-css/main.css
```

## Note

If the shared css change, publish with git, and update others package.json are using the shared css

- Pull last change in master branch
```
npm install git+ssh://bitbucket.org:MiguelSavignano/shared-css.git --save
```

- Pull last change by branch, tag, commit hash
```
npm install git+ssh://bitbucket.org:MiguelSavignano/shared-css.git#branch-name --save
```
